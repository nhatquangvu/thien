<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function server(){
        return $this->hasOne('App\Server','id','server_id');
    }
    public function user(){
        return  $this->hasOne('App\User','id','user_id');
    }
}
