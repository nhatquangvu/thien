<?php

namespace App\Providers;

use App\Planet;
use App\Server;
use App\System;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $system = System::find(1);
        $servers = Server::all();
        $planets = Planet::all();
        View::share(['system' => $system, 'servers' => $servers, 'planets' => $planets]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
