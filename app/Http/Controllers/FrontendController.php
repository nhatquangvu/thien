<?php

namespace App\Http\Controllers;

use App\Account;
use App\Card;
use App\LogTrans;
use App\Service;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class FrontendController extends Controller
{
    public function index(Request $request)
    {
        return view('index');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return Redirect::route('index');
    }

    public function login(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('login');
        } else {
            if (Auth::attempt(['name' => $request->name, 'password' => $request->password])) {
                return Redirect::route('index');
            } else {
                return Redirect::route('login')->with('notice', 'Tài khoản hoặc mật khẩu không đúng');
            }
        }
    }

    public function register(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('register');
        } else {
            if ($request->password <> $request->repassword) {
                return Redirect::route('register')->with('notice', 'Gõ lại mật khẩu không chính xác');
            }
            $name = str_slug($request->get('name'), '-');
            if (!User::where('name', $name)->first()) {
                $user = new User();
                $user->name = $name;
                $user->password = bcrypt($request->password);
                $user->save();
                return Redirect::route('login')->with('notice', 'Đăng ký thành công : ' . $name);

            } else {
                return Redirect::route('register')->with('notice', 'Tài khoản đã tồn tại ');

            }
        }
    }

    public function card(Request $request)
    {

        if ($request->isMethod('get')) {
            if (Auth::check()) {
                if ($request->has('card_id')) {
                    $card = Card::find($request->card_id);
                    if ($card->isDone && $card->isCorrect)
                        return '<span class="badge badge-success">Thành công</span>';
                    elseif ($card->isDone && $card->isCorrect == 0)
                        return '<span class="badge badge-danger">Thẻ hủy</span>';
                    elseif ($card->isDone == 0)
                        return '<span class="badge badge-warning">Đang xử lý</span>';

                }
                $myCards = Card::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(20);
                return view('card', ['myCards' => $myCards]);
            } else {
                return Redirect::route('login');
            }
        } else {
            if (strlen($request->serial) < 6 or strlen($request->key) < 9) {
                return Redirect::route('card')->with('notice', 'Nhập chính xác serial và mã thẻ ! Nhập sai nhiều lần sẽ bị khóa tài khoản');
            }
            $card = new Card();
            $card->network = $request->network;
            $card->price = $request->price;
            $card->serial = $request->serial;
            $card->key = $request->key;
            $card->user_id = Auth::user()->id;
            $card->save();
            return Redirect::route('card')->with('notice', 'Nạp thẻ thành công !');

        }
    }

    public function gold(Request $request)
    {
        if ($request->isMethod('get')) {
            if ($request->has('service_id')) {
                return Service::find($request->service_id)->message;
            }
            if (Auth::check()) {
                $services = Service::where('user_id', Auth::user()->id)->where('isGold', 1)->paginate(20);
                return view('gold', ['services' => $services]);
            } else {
                return view('gold');
            }
        } else {
            $user = Auth::user();
            if ($request->balance > $user->balance) {
                return Redirect::route('gold')->with('notice', 'Bạn không đủ tiền để giao dịch');
            } else {
                if ($request->balance > 0) {
                    $user->balance -= $request->balance;
                    $user->save();
                    $ser = new Service();
                    $ser->server_id = $request->get('server');
                    $ser->planet_id = $request->get('planet');
                    $ser->account = $request->get('account');
                    $ser->password = $request->get('password');
                    $ser->quality = $request->get('quality');
                    $ser->balance = $request->get('balance');
                    $ser->message = 'Đang chờ';
                    $ser->user_id = $user->id;
                    $ser->save();
                    return Redirect::route('gold')->with('notice', 'Giao dịch thành công');
                }
            }
        }
    }

    public function gem(Request $request)
    {
        if ($request->isMethod('get')) {
            if ($request->has('service_id')) {
                return Service::find($request->service_id)->message;
            }

            if (Auth::check()) {
                $services = Service::where('user_id', Auth::user()->id)->where('isGold', 0)->paginate(20);
                return view('gem', ['services' => $services]);
            } else {
                return view('gem');
            }
        } else {
            $user = Auth::user();
            if ($request->balance > $user->balance) {
                return Redirect::route('gem')->with('notice', 'Bạn không đủ tiền để giao dịch');
            } else {
                if ($request->balance > 0) {
                    $user->balance -= $request->balance;
                    $user->save();
                    $ser = new Service();
                    $ser->server_id = $request->get('server');
                    $ser->planet_id = $request->get('planet');
                    $ser->account = $request->get('account');
                    $ser->password = $request->get('password');
                    $ser->quality = $request->get('quality');
                    $ser->balance = $request->get('balance');
                    $ser->isGold = 0;
                    $ser->message = 'Đang chờ';
                    $ser->user_id = $user->id;
                    $ser->save();


                    return Redirect::route('gem')->with('notice', 'Giao dịch thành công');
                }


            }
        }
    }

    public function shop(Request $request)
    {
        if ($request->isMethod('get')) {
            if ($request->has('buy')) {
                $acc = Account::find($request->get('buy'));
                if ($acc->user_id <> 0) {
                    return Redirect::route('shop')->with('notice', 'Tài khoản đã bán');
                } else {
                    if ($acc->price > Auth::user()->balance) {
                        return Redirect::route('shop')->with('notice', 'Bạn không đủ tiền để mua acc này');
                    } else {
                        Auth::user()->balance -= $acc->price;
                        Auth::user()->save();
                        $acc->user_id = Auth::id();
                        $acc->save();
                        return Redirect::route('shop')->with('notice', 'Mua acc thành công');
                    }
                }
            }
            if ($request->has('server')) {
                if ($request->get('numb') == NULL) {
                    $pri = explode(',', $request->get('price'));
                    $query = Account::query();
                    $query->where('price', '<=', $pri[1]);
                    $query->where('price', '>=', $pri[0]);
                    if ($request->hasPract == 1) {
                        $query->where('hasPract', 1);
                        $query->where('isNewBorn', 1);
                    }
                    if ($request->hasRing == 1) {
                        $query->where('hasRing', 1);
                    }
                    if ($request->get('server') <> 0) {
                        $query->where('server_id', $request->get('server'));
                    }
                    if ($request->get('planet') <> 0) {
                        $query->where('planet_id', $request->get('planet'));
                    }
                    $query->where('user_id', 0);
                    $accs = $query->paginate(20);
                } else {
                    $accs = Account::where('id', $request->get('numb'))->where('user_id', 0)->paginate(20);
                }
            } else {
                $accs = Account::orderBy('id', 'desc')->where('user_id', 0)->paginate(12);
            }
            $myaccs = Account::where('user_id', Auth::id())->paginate(12);
            return view('shop', ['accs' => $accs, 'myaccs' => $myaccs]);
        }
    }

    public function send(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('send');
        } else {
            $user = User::where('name', $request->get('name'))->first();
            if ($user->id <> Auth::id()) {
                if (!$user) {
                    return route('send')->with('notice', 'Thành viên không tồn tại');
                } else {
                    if (Auth::user()->balance < $request->get('balance') || $request->get('balance') < 0) {
                        return Redirect::route('send')->with('notice', 'Số tiền của bạn không đủ');
                    } else {
                        Auth::user()->balance -= $request->get('balance');
                        $user->balance += $request->get('balance');
                        $user->save();
                        Auth::user()->save();
                        $log_trans = new LogTrans();
                        $log_trans->user_id = Auth::id();
                        $log_trans->content = 'Bạn đã chuyển : <span class="badge badge-danger">' . number_format($request->get('balance')) . '</span> cho tài khoản ' . $user->name . ' vào lúc ' . Carbon::now()->format('H:i d/m/Y');
                        $log_trans->save();
                        return Redirect::route('send')->with('notice', 'Chuyển tiền thành công');
                    }
                }
            }
        }
    }
}
