<?php

namespace App\Http\Controllers;

use App\Account;
use App\Card;
use App\Service;
use App\System;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class BackendController extends Controller
{
    public function member(Request $request)
    {
        if ($request->isMethod('get')) {
            if($request->has('banned')){
                $user = User::find($request->get('banned'));
                $user->isBanned = !$user->isBanned;
                $user->save();
                return Redirect::route('manager::member');
            }
            if($request->has('search')){
                $users = User::where('name', 'like', '%'.$request->get('search').'%')->paginate(50);
                return view('admin.member', ['users' => $users]);
            }
            $users = User::where('id', '<>', 1)->paginate(50);
            return view('admin.member', ['users' => $users]);
        } else {
            if ($request->has('user_add')) {
                $user = User::find($request->get('user_add'));
                $user->balance += $request->get('plus');
                $user->save();
                return Redirect::route('manager::member')->with('notice', 'Nạp tiền thành công');
            }
        }
    }

    public function card(Request $request)
    {
        if ($request->isMethod('get')) {
            if ($request->has('check')) {
                $card = Card::find($request->get('check'));
                if(!$card->isDone){
                    $card->isDone = 1;
                    $card->isCorrect = 1;
                    $card->save();
                    $card->user->balance += $card->price;
                    $card->user->save();
                }
            }
            if ($request->has('correct')) {
                $card = Card::find($request->get('correct'));
                if(!$card->isDone){
                    $card->isDone = 1;
                    $card->isCorrect = 0;
                    $card->save();
                }
            }
            $cards = Card::orderBy('id','desc')->paginate(100);
            return view('admin.card', ['cards' => $cards]);
        }
    }
    public function services(Request $request){
        if($request->isMethod('get')){
            if($request->has('check')){
              $ser =   Service::find($request->get('check'));
              if(!$ser->isDone){
                  $ser->isDone = 1;
                  $ser->save();
              }
            }
            if($request->has('del')){
              $ser =   Service::find($request->get('del'));
              if($ser && !$ser->isDone){
                  $ser->user->balance += $ser->balance;
                  $ser->user->save();
                  $ser->delete();
              }
            }
            $services = Service::orderBy('id','desc')->paginate(200);
            return view('admin.services',['services' => $services]);
        }else{
            $ser = Service::find($request->get('id'));
            $ser->message = $request->get('message');
            $ser->save();
            return $ser->message;
        }
    }
    public function shop(Request $request){
        if($request->isMethod('get')){
            if($request->has('del')){
                $acc = Account::find($request->get('del'));
                if($acc){
                    if($acc->user_id == 0){
                        $acc->delete();
                        return Redirect::route('manager::shop')->with('notice','Đã xóa tài khoản');
                    }else{
                        return Redirect::route('manager::shop')->with('notice','Tài khoản đã được bán, không thể xóa');

                    }
                }
            }
            if($request->has('add')){
                return view('admin.shop',['add' =>1]);
            }
            if($request->has('search')){
                $accs = Account::OrderBy('id','desc')->where('id',$request->get('search'))->get();
                return view('admin.shop',['accs' => $accs]);
            }
            $accs = Account::OrderBy('id','desc')->get();
            return view('admin.shop',['accs' => $accs]);
        }else{
            $acc = new Account();
            $acc->name = $request->name;
            $acc->server_id = $request->server_id;
            $acc->planet_id = $request->planet_id;
            $acc->account = $request->account;
            $acc->password = $request->password;
            $acc->hasRing = $request->hasRing;
            $acc->price = $request->price;
            $acc->hasGmail = $request->hasGmail;
            $acc->hasPract = $request->hasPract;
            $acc->isNewBorn = $request->isNewBorn;
            $acc->content = $request->get('content');
            $photoName = time().'.'.Input::file('image')->getClientOriginalExtension();
            Input::file('image')->move(public_path('images/account'), $photoName);
            $acc->image = $photoName;
            $acc->save();
            return Redirect::route('manager::shop')->with('notice','Thêm tài khoản thành công');

        }
    }
    public function system(Request $request){
        if($request->isMethod('get')){
            return view('admin.system');
        }else{
            System::where('id',1)->update([
                'rate_gold' => $request->rate_gold,
                'rate_gem' => $request->rate_gem,
                'hot_line' => $request->hot_line,
                'facebook' => $request->facebook,
                'youtube' => $request->youtube,
                'message' => $request->get('message'),
            ]);
            return Redirect::route('manager::system');
        }
    }
}
