<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function server(){
        return $this->hasOne('App\Server','id','server_id');
    }
    public function planet(){
        return $this->hasOne('App\Planet','id','planet_id');
    }
    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
}
