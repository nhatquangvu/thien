@extends('admin.template')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tìm kiếm</h4>
                    <form action="{{route('manager::member')}}" method="get" id='search'>
                        <div class="form-group">
                          <label for="">Tên thành viên</label>
                          <input type="text" name="search" id="" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                        <div class="form-group">
                          <button class='btn btn-xs btn-primary' form='search' type="submit"><i class="fa fa-search"></i> Tìm kiếm</button>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Quản lý người dùng</h4>
                    
                    <table id="myTable" class="display nowrap table table-hover table-striped table-bordered"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên tài khoản</th>
                            <th>Tài khoản</th>
                            <th>Banned</th>
                            <th>Thêm tiền</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Tên tài khoản</th>
                            <th>Tài khoản</th>
                            <th>Banned</th>
                            <th>Thêm tiền</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{number_format($user->balance)}}</td>
                                @if($user->isBanned)
                                    <td><a href="{{route('manager::member',['banned'=> $user->id])}}" class="badge badge-danger">Banned</a></td>
                                    @else
                                    <td><a href="{{route('manager::member',['banned'=>  $user->id])}}" class="badge badge-warning">Bình thường</a></td>
                                @endif
                                <td width="200px">
                                    <form action="{{route('manager::member')}}" method="post">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-plus"></i></button>
                                            </div>
                                            <input type="hidden" name="user_add" value="{{$user->id}}">
                                            {{csrf_field()}}
                                            <input type="text" class="form-control" name="plus" required placeholder="" aria-label="" aria-describedby="basic-addon1">
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $users->links('pagination::bootstrap-4') !!}
                </div>
            </div>
        </div>
    </div>
@endsection