@extends('admin.template')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Quản lý giao dịch</h4>
                    <table id="myTable" class="display nowrap table table-hover table-striped table-bordered"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Server</th>
                            <th>Tài khoản</th>
                            <th>Mật khẩu</th>
                            <th>Số lượng</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Server</th>
                            <th>Tài khoản</th>
                            <th>Mật khẩu</th>
                            <th>Số lượng</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>

                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($services as $service)
                            <tr>
                                <td>{{$service->id}}</td>

                                <td>{{$service->server->name}}</td>
                                <td>{{$service->account}}</td>
                                <td>{{$service->password}}</td>
                                @if($service->isGold == 1)
                                    <td><span class="badge badge-warning">{{number_format($service->quality)}}
                                            Vàng</span></td>
                                @else
                                    <td><span class="badge badge-info">{{number_format($service->quality)}} Ngọc</span>
                                    </td>
                                @endif
                                @if($service->isDone)
                                    <td>Hoàn thành</td>
                                @else
                                    <td>
                                        <a href="{{route('manager::services',['check' => $service->id])}}" class="badge badge-success">Hoàn thành</a>
                                        <a href="{{route('manager::services',['del' => $service->id])}}" class="badge badge-danger">Từ chối</a>
                                    </td>

                                @endif
                                @if($service->isDone)
                                    <td>Hoàn thành</td>
                                @else
                                    <td>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-outline-secondary" type="submit"><i
                                                            class="fa fa-comment-o"></i></button>
                                            </div>
                                            {{csrf_field()}}
                                            <input type="text" class="form-control sendMes" name="sendMes"
                                                   data-id="{{$service->id}}" id="mess-{{$service->id}}" value="{{$service->message}}">
                                        </div>
                                    </td>

                                @endif

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        $('.sendMes').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                id_ser = $(this).attr('data-id');
                mess = $(this).val();
                $.ajax({
                    url : "{{route('manager::services')}}",
                    type : "post",
                    dataType:"text",
                    data : {
                        "_token": "{{ csrf_token() }}",
                        "id": id_ser,
                        "message" : mess
                    },
                    success : function (result){
                        $('#mess-' + id_ser).val(result);
                        console.log('thành công')
                    }
                });
            }
        });
    </script>
    @endsection