@extends('admin.template')
@section('content')
    <div class="row">
        <div class="col-12">

        <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tìm kiếm</h4>
                    <form action="{{route('manager::shop')}}" method="GET" id='search'>
                        <div class="form-group">
                          <label for="">Tìm kiếm mã</label>
                          <input type="text" name="search" id="" class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                          <button class='btn btn-xs btn-primary' form='search' type="submit"><i class="fa fa-search"></i> Tìm kiếm</button>
                    </form>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Quản lý shop bán tài khoản</h4>
                    @if(!isset($add))
                        <a href="{{route('manager::shop',['add'=> 1])}}" class="btn btn-primary float-right"><i
                                    class="fa fa-plus"></i> Thêm ACC</a>
                        <table id="myTable" class="display nowrap table table-hover table-striped table-bordered"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Tên tài khoản</th>
                                <th>Mật khẩu</th>
                                <th>Giá</th>
                                <th>Server</th>
                                <th>Trạng thái</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Tên tài khoản</th>
                                <th>Mật khẩu</th>
                                <th>Giá</th>
                                <th>Server</th>
                                <th>Trạng thái</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($accs as $acc)
                                <tr>
                                    <td>{{$acc->id}}</td>
                                    <td>{{$acc->account}}</td>
                                    <td>{{$acc->password}}</td>
                                    <td>{{$acc->price}}</td>
                                    <td>{{$acc->server->name}}</td>
                                    <td>
                                        @if($acc->user_id <> 0)
                                    <span class="badge badge-danger">Đã bán : {{$acc->user->name}}</span>
                                        @else
                                            <span class="badge badge-primary">Chưa bán</span>
                                            <a href="{{route('manager::shop',['del' => $acc->id])}}" class="badge badge-danger"><i class=" fa fa-trash"></i> Xóa</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <a href="{{route('manager::shop')}}" class="btn btn-primary float-right"><i
                                    class="fa fa-plus"></i> Quản lý ACC</a>

                        <form action="{{route('manager::shop')}}" method="post" enctype="multipart/form-data"
                              class="mt-5">
                            <h4>Thêm tài khoản mới</h4>
                            <hr>
                            <label for="">Giới thiệu tài khoản</label>
                            <input type="text" name="name" class="form-control" required>
                            <label for="">Tài khoản</label>
                            <input type="text" name="account" class="form-control" required>
                            <label for="">Mật khẩu</label>
                            <input type="text" name="password" class="form-control" required>
                            <label for="">Giá</label>
                            <input type="number" name="price" class="form-control" required>
                            <hr>
                            <label for="">Ảnh đại diện</label>
                            <input type="file" name="image" class="form-control" required>
                            <label for="">Chọn server</label>
                            <select name="server_id" id="" class="form-control" required>
                                @foreach($servers as $server)
                                    <option value="{{$server->id}}">{{$server->name}}</option>
                                @endforeach
                            </select>
                            <label for="">Chọn hành tinh</label>
                            <select name="planet_id" id="" class="form-control" required>
                                @foreach($planets as $planet)
                                    <option value="{{$planet->id}}">{{$planet->name}}</option>
                                @endforeach
                            </select>
                            <label for="">Kiểu đăng ký</label>
                            <select name="hasGmail" id="" class="form-control">
                                <option value="0" selected>Tài khoản ảo</option>
                                <option value="1">Full Gmail</option>
                            </select>
                            <label for="">Sơ Sinh có đệ</label>
                            <select name="hasPract" id="" class="form-control">
                                <option value="0" selected>Không</option>
                                <option value="1">Có đệ</option>
                            </select>
                            <label for="">Bông tai</label>
                            <select name="hasRing" id="" class="form-control">
                                <option value="0" selected>Không</option>
                                <option value="1">Có Bông tai</option>
                            </select>
                            <label for="">Nick Sơ Sinh</label>
                            <select name="isNewBorn" id="" class="form-control">
                                <option value="0" selected>Không</option>
                                <option value="1">Nick sơ sinh</option>
                            </select>
                            <label for="">Chi tiết</label>
                            <textarea id="my-editor" name="content" class="form-control"></textarea>

                            {{csrf_field()}}
                            <br>
                            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-upload"></i> Thêm
                                nick
                            </button>
                        </form>


                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection