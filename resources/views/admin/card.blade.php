@extends('admin.template')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Quản lý thẻ cào</h4>
                    <table id="myTable" class="display nowrap table table-hover table-striped table-bordered"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nhà mạng</th>
                            <th>Mệnh giá</th>
                            <th>Serial</th>
                            <th>Key</th>
                            <th>Từ</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                            <th>Vào lúc</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Nhà mạng</th>
                            <th>Mệnh giá</th>
                            <th>Serial</th>
                            <th>Key</th>
                            <th>Từ</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                            <th>Vào lúc</th>

                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($cards as $card)
                            <tr>
                                <td>{{$card->id}}</td>
                                <td>{{$card->network}}</td>
                                <td>{{number_format($card->price)}}</td>
                                <td>{{$card->serial}}</td>
                                <td>{{$card->key}}</td>
                                <td>{{$card->user->name}}</td>
                                @if(!$card->isDone)
                                   <td>Đang xử lý</td>
                                @else
                                    @if($card->isCorrect)
                                        <td><span class="badge badge-success">Thành công</span></td>
                                        @else
                                        <td><span class="badge badge-danger">Thẻ sai</span></td>
                                    @endif
                                @endif
                                @if(!$card->isDone)
                                    <td>
                                        <a href="{{route('manager::card',['check' => $card->id])}}"
                                           class="badge badge-success text-white">Báo đúng</a>
                                        <a href="{{route('manager::card',['correct' => $card->id])}}"
                                           class="badge badge-danger text-white">Báo sai</a>
                                    </td>
                                    @else
                                    <td>Xử lý xong</td>
                                @endif
                                <td>{{$card->created_at->format('H:i d/m/Y')}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $cards->links('pagination::bootstrap-4') !!}
                </div>
            </div>
        </div>
    </div>
@endsection