@extends('admin.template')
@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Quản lý hệ thông</h4>
                    <form action="{{route('manager::system')}}" method="post">
                        <div class="form-group">
                            <label>Tỉ giá vàng</label>
                            <input type="text" class="form-control" name="rate_gold" value="{{$system->rate_gold}}">
                        </div>
                        <div class="form-group">
                            <label for="">Tỉ giá ngọc</label>
                            <input type="text" class="form-control" name="rate_gem" value="{{$system->rate_gem}}">
                        </div>
                        <div class="form-group">
                            <label for="">Hotline</label>
                            <input type="text" class="form-control" name="hot_line" value="{{$system->hot_line}}">
                        </div>
                        <div class="form-group">
                            <label for="">Link Facebook</label>
                            <input type="text" class="form-control" name="facebook" value="{{$system->facebook}}">
                        </div>
                        <div class="form-group">
                            <label for="">Link Youtube</label>
                            <input type="text" class="form-control" name="youtube" value="{{$system->youtube}}">
                        </div>
                        <label for="">Chi tiết</label>
                        <textarea id="my-editor" name="message" class="form-control"> {{$system->message}}</textarea>
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-lg btn-primary btn-block mt-2">Cập nhập</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection