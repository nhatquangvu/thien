@extends('template')
@section('content')
    <div id="gold">
        <div class="container p-0">
            <div class="row m-0 p-0">
                @if(\Illuminate\Support\Facades\Session::has('notice'))
                    <div class="col-sm-12 m-0 p-0">
                        <p class="bg-success text-white p-3">
                            {{\Illuminate\Support\Facades\Session::get('notice')}}
                        </p>
                    </div>
                @endif
                <div class="col-sm-6 col-12 m-0 p-0">
                    <div class="bg-trans p-4">
                        <h4 class="header"><img src="images/mini-icon.png" alt=""> MUA VÀNG</h4>

                        <form action="{{route('gold')}}" method="post">
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Server</label>
                                <select name="server" class="form-control">
                                    @foreach($servers as $server)
                                        <option value="{{$server->id}}">{{$server->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Hành tinh</label>
                                <select name="planet" class="form-control">
                                    @foreach($planets as $planet)
                                        <option value="{{$planet->id}}">{{$planet->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Tài khoản</label>
                                <input type="text" class="form-control" name="account" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Mật khẩu</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Số tiền</label>
                                <input type="number" class="form-control" id="buy" required name="balance">
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Hệ số x</label>
                                <input type="string" class="form-control" readonly value="{{$system->rate_gold}}">
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Số vàng nhận</label>
                                <input type="number" class="form-control" readonly name="quality" id="quality">
                            </div>
                            @if(\Illuminate\Support\Facades\Auth::check())
                                {{csrf_field()}}

                                <button class="btn btn-warning btn-block text-uppercase text-white"><i
                                            class="fa fa-diamond"></i> mua vàng
                                </button>
                            @else
                                <a href="{{route('login')}}"
                                   class="btn btn-login btn-block text-uppercase text-white"><i
                                            class="fa fa-sign-in"></i> Đăng nhập để mua vàng
                                </a>
                            @endif
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 col-12 m-0 p-0 bg-trans text-white">
                    <div class="p-3">
                        <h5><i class="fa fa-bell"></i> Hướng dẫn</h5>
                        <hr class="white">
                        <p>- Giá vàng VT 4, 6, 7 | 10.000 nhận ngay {{number_format(10000 * $system->rate_gold)}} vàng
                            Sau khi nạp thẻ lúc nào cũng có thể giao
                            dịch
                            <br>- Chú ý: Nếu nhập sai tài khoản hoặc server hệ thống sẽ tự động hoàn tiền sau 10 phút
                            khách không lo mất tiền nhé !
                            <br>- Chúc các bạn chơi game vui vẻ !</p>
                    </div>
                </div>

                @if(\Illuminate\Support\Facades\Auth::check())
                    <div class="col-sm-12 mt-3 m-0 p-0">
                        <h5 class="text-white"><i class="fa fa-id-card"></i> Lịch sử vàng</h5>
                        <table class="table table-striped table-dark">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Server</th>
                                <th scope="col">Tài khoản</th>
                                <th scope="col">Số Vàng</th>
                                <th scope="col">Tình trạng</th>
                                <th scope="col" width="350px">Tin hệ thống</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($services as $service)
                                <tr>
                                    <td>{{$service->id}}</td>
                                    <td>{{$service->server->name}}</td>
                                    <td>{{$service->account}}</td>
                                    <td>{{number_format($service->quality)}}</td>
                                    @if($service->isDone == 0)
                                        <td><span class="badge badge-warning">Đang chờ</span></td>
                                        <script>
                                            setInterval(function () {
                                                $.ajax({
                                                    url: "{{route('gold')}}",
                                                    type: "GET",
                                                    dataType: "text",
                                                    data: {
                                                        "_token": "{{csrf_token()}}",
                                                        "service_id": "{{$service->id}}"
                                                    },
                                                    success: function (result) {
                                                        $('#mes-{{$service->id}}').html(result);
                                                    }
                                                });

                                            }, 5000)

                                        </script>
                                    @else
                                        <td><span class="badge badge-success">Thành công</span></td>
                                    @endif
                                    <td><i class="fa fa-bullhorn"></i> <span id="mes-{{$service->id}}">{{$service->message}}</span></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        {!! $services->links('pagination::bootstrap-4') !!}

                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#buy').change(function () {
            var result = $('#buy').val() * {{$system->rate_gold}};
            $('#quality').val(result);
        });

    </script>
@endsection