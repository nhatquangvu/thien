@extends('template')
@section('content')
    <div id="gold">
        <div class="container p-0">
            <div class="row m-0 p-0">
                @if(\Illuminate\Support\Facades\Session::has('notice'))
                    <div class="col-sm-12 m-0 p-0">
                        <p class="bg-success text-white p-3">
                            {{\Illuminate\Support\Facades\Session::get('notice')}}
                        </p>
                    </div>
                @endif
                <div class="col-sm-6 col-12 m-0 p-0">
                    <div class="bg-trans p-4">
                        <h4 class="header"><img src="images/mini-icon.png" alt=""> MUA NGỌC</h4>

                        <form action="{{route('gem')}}" method="post">
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Server</label>
                                <select name="server" class="form-control">
                                    @foreach($servers as $server)
                                        <option value="{{$server->id}}">{{$server->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Hành tinh</label>
                                <select name="planet" class="form-control">
                                    @foreach($planets as $planet)
                                        <option value="{{$planet->id}}">{{$planet->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Tài khoản</label>
                                <input type="text" class="form-control" name="account" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Mật khẩu</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Số tiền</label>
                                <input type="number" class="form-control" id="buy" required name="balance" min="50000">
                                <span class="badge badge-danger"><i class="fa fa-warning"></i> Tối thiểu 50.000</span>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Hệ số x</label>
                                <input type="string" class="form-control" readonly value="{{$system->rate_gem}}">
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Số ngọc nhận</label>
                                <input type="number" class="form-control" readonly name="quality" id="quality">
                            </div>
                            @if(\Illuminate\Support\Facades\Auth::check())
                                {{csrf_field()}}

                                <button class="btn btn-info btn-block text-uppercase text-white"><i
                                            class="fa fa-diamond"></i> mua ngọc
                                </button>
                            @else
                                <a href="{{route('login')}}"
                                   class="btn btn-login btn-block text-uppercase text-white"><i
                                            class="fa fa-sign-in"></i> Đăng nhập để mua vàng
                                </a>
                            @endif
                        </form>
                    </div>
                </div>
                <div class="col-sm-6 col-12 m-0 p-0 bg-trans text-white">
                    <div class="p-3">
                        <h5><i class="fa fa-bell"></i> Hướng dẫn</h5>
                        <hr class="white">
                        <p>
                            - Hệ thống bán ngọc xanh Tự Động 100%, Mật khẩu được bảo mật an toàn tuyệt đối, không sợ mất
                            nick (Nếu bạn chưa biết cách giao dịch hãy xem hướng dẫn mua ngọc)
                            <br> - Giá ngọc x{{$system->rate_gem * 1000}} , tức 100k kí
                            gửi {{$system->rate_gem * 1000 * 100}} Bán từ 50k trở lên
                            <br> - Khi mua ngọc hãy tải lại trang xem phần Lịch Sử Mua Ngọc theo dõi Trạng Thái để biết
                            tiến trình giao dịch
                            <br> - Hệ thống báo Giao dịch thất bại (Không tìm thấy nhân vật, Nhận item lỗi) các bạn thực
                            hiện lại giao dịch là được nhé.
                            <br> - Yều cầu trước khi mua ngọc:
                            <br> 1. Nhân vật phải đến sẵn Siêu Thị và thoát ra trước khi mua ngọc tại đây
                            <br> 2. Có ít nhất 1 ngọc để kí gửi.
                            <br> 3. Hành trang phải có ít nhất 1 chỗ trống.
                            <br> 4. Không cài mật khẩu rương.
                            <br> - Nếu không thỏa mãn 4 yêu cầu trên thì không thể nhận ngọc và hệ thống sẽ tự động hoàn
                            lại tiền vào tài khoản
                        </p>
                    </div>
                </div>

                @if(\Illuminate\Support\Facades\Auth::check())
                    <div class="col-sm-12 mt-3 m-0 p-0">
                        <h5 class="text-white"><i class="fa fa-id-card"></i> Lịch sử vàng</h5>
                        <table class="table table-striped table-dark">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Server</th>
                                <th scope="col">Tài khoản</th>
                                <th scope="col">Số Vàng</th>
                                <th scope="col">Tình trạng</th>
                                <th scope="col" width="350px">Tin hệ thống</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($services as $service)
                                <tr>
                                    <td>{{$service->id}}</td>
                                    <td>{{$service->server->name}}</td>
                                    <td>{{$service->account}}</td>
                                    <td>{{number_format($service->quality)}}</td>
                                    @if($service->isDone == 0)
                                        <td><span class="badge badge-warning">Đang chờ</span></td>
                                        <script>
                                            setInterval(function () {
                                                $.ajax({
                                                    url: "{{route('gold')}}",
                                                    type: "GET",
                                                    dataType: "text",
                                                    data: {
                                                        "_token": "{{csrf_token()}}",
                                                        "service_id": "{{$service->id}}"
                                                    },
                                                    success: function (result) {
                                                        $('#mes-{{$service->id}}').html(result);
                                                    }
                                                });

                                            }, 5000)

                                        </script>
                                    @else
                                        <td><span class="badge badge-success">Thành công</span></td>
                                    @endif
                                    <td><i class="fa fa-bullhorn"></i> <span id="mes-{{$service->id}}">{{$service->message}}</span></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        {!! $services->links('pagination::bootstrap-4') !!}

                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#buy').change(function () {
            var result = $('#buy').val() * {{$system->rate_gem}};
            $('#quality').val(result);
        });

    </script>
@endsection