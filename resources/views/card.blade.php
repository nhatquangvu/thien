@extends('template')
@section('content')
    <div id="login">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-sm-3">
                    <h4 class="header"><img src="images/mini-icon.png" alt=""> Thẻ nạp</h4>
                    <div class="bg-trans p-4">
                        <form action="{{route('card')}}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Nhà mạng</label>
                                <select name="network" id="" class="form-control">
                                    <option value="Viettel">Viettel</option>
                                    <option value="Vinaphone">Vinaphone</option>
                                    <option value="Mobiphone">Mobiphone</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Mệnh giá</label>
                                <select name="price" id="" class="form-control">
                                    <option value="20000">20.000</option>
                                    <option value="50000">50.000</option>
                                    <option value="100000">100.000</option>
                                    <option value="200000">200.000</option>
                                    <option value="500000">500.000</option>
                                    <option value="1000000">1.000.000</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Mã thẻ</label>
                                <input type="text" class="form-control" name="key">
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Serial</label>
                                <input type="text" class="form-control" name="serial">
                            </div>
                            
                            {{csrf_field()}}
                            <button class="btn btn-warning btn-block text-uppercase text-white"><i
                                        class="fa fa-credit-card-alt"></i> Nạp thẻ </button>
                        </form>
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <br>
                            <p class="bg-success text-white p-3">
                                {{\Illuminate\Support\Facades\Session::get('notice')}}
                            </p>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 mt-3">
                    <h5 class="text-white"><i class="fa fa-id-card"></i> Lịch sử nạp thẻ</h5>
                    <table class="table table-striped table-dark">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Mạng</th>
                            <th scope="col">Mệnh giá</th>
                            <th scope="col">Serial</th>
                            <th scope="col">Mã thẻ</th>
                            <th scope="col">Tình trạng</th>
                            <th scope="col">Thời gian</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($myCards as $card)
                            <tr>
                                <th scope="row">{{$card->id}}</th>
                                <td>{{$card->network}}</td>
                                <td>{{number_format($card->price)}}</td>
                                <td>{{$card->serial}}</td>
                                <td>{{$card->key}}</td>
                                @if($card->isDone && $card->isCorrect)
                                    <td id="card-{{$card->id}}"><span class="badge badge-success">Thành công</span></td>
                                @elseif($card->isDone && $card->isCorrect == 0)
                                    <td id="card-{{$card->id}}"><span class="badge badge-danger">Thẻ sai</span></td>
                                @else
                                    <td id="card-{{$card->id}}"><span class="badge badge-warning">Đang xử lý</span></td>
                                    <script>

                                        setInterval(function () {
                                            $.ajax({
                                                url: "{{route('card')}}",
                                                type: "GET",
                                                dataType: "text",
                                                data: {
                                                    "_token": "{{csrf_token()}}",
                                                    "card_id": "{{$card->id}}"
                                                },
                                                success: function (result) {
                                                    $('#card-{{$card->id}}').html(result);
                                                }
                                            });

                                        }, 5000)

                                    </script>
                                @endif
                                <td>{{$card->created_at->format('H:i d/m')}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $myCards->links('pagination::bootstrap-4') !!}
                </div>
            </div>
        </div>
    </div>
@endsection