@extends('template')
@section('content')
    <div id="login">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-sm-3">
                    <h4 class="header"><img src="images/mini-icon.png" alt=""> Gửi tiền</h4>
                    <div class="bg-trans p-4">
                        <form action="{{route('send')}}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Tên thành viên</label>
                                <input type="text" class="form-control" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Số tiền</label>
                                <input type="number" class="form-control" name="balance" required>
                            </div>
                            {{csrf_field()}}
                            <button class="btn btn-login btn-block text-uppercase text-white"><i
                                        class="fa fa-send-o"></i> Gửi
                            </button>
                        </form>
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <br>
                            <p class="bg-success text-white p-3">
                                {{\Illuminate\Support\Facades\Session::get('notice')}}
                            </p>
                        @endif
                    </div>
                </div>
                @if(\Illuminate\Support\Facades\Auth::check())
                    <div class="col-sm-12 mt-3 m-0 p-0">
                        <h5 class="text-white"><i class="fa fa-id-card"></i> Lịch sử giao dịch</h5>
                        <table class="table table-striped table-dark">
                            <thead>
                            <tr>
                                <th scope="col">#ID</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\LogTrans::where('user_id',\Illuminate\Support\Facades\Auth::id())->orderBy('id','desc')->get() as $item)
                                <tr>
                                    <td><span class="badge badge-danger">#{{$item->id}}</span></td>
                                    <td>{!! $item->content !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>

                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection