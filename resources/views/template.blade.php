<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shop vàng - Shop ngọc - Shop nick  tự động giá tốt  lung2linh - 18cuhanh - uy tín là hàng đầu</title>
    <meta property="og:title" content="Shop acc tự động lung2linh - 18cuhanh - Tự động và uy tín hàng đầu">
        <meta property="og:description" content="{{strip_tags($system->message,'<p>')}}">
    <meta property="og:image" content="https://lung2linh.vn/images/s2.jpg">
    <meta property="og:url" content="https://lung2linh.vn">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover-min.css"/>
</head>
<body>
<div id="top">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <i class="fa fa-bell-o"></i> Hotline : {{$system->hot_line}} <br>
                <span class="social-icons">
                    <a href="{{$system->facebook}}"><i class="fa fa-facebook-official"></i>/lung2linh</a>
                    <a href="{{$system->youtube}}"><i class="fa fa-youtube-play"></i>/lung2linh</a>
                </span>
            </div>
        </div>
       
    </div>
</div>
<div id="logo">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-5">
                <img src="images/logo.png" alt="" class="img-fluid mt-4 mb-4">
            </div>
            <div class="col-sm-3 offset-sm-5 col-7 p-4">
                @if(!\Illuminate\Support\Facades\Auth::check())
                    <a href="{{route('login')}}"
                       class="btn btn-xs btn-login btn-block btn-success text-uppercase font-weight-bold"><i
                                class="fa fa-sign-in"></i> Đăng nhập</a>
                    <a href="{{route('register')}}"
                       class="btn btn-xs btn-reg btn-block btn-danger text-uppercase font-weight-bold"><i
                                class="fa fa-registered"></i> Đăng ký</a>
                @else
                    <p class="text-white">
                        Xin chào , {{\Illuminate\Support\Facades\Auth::user()->name}} <br>
                        <span class="badge badge-success">TK : {{number_format(\Illuminate\Support\Facades\Auth::user()->balance)}}</span>
                        <a href="{{route('logout')}}" class="badge badge-danger"><i class="fa fa-sign-out"></i> Thoát</a>
                        @if(\Illuminate\Support\Facades\Auth::id() == 1)
                            <br>
                            <a href="{{route('manager::card')}}" class="text-white text-uppercase"><i class="fa fa-key"></i> Trang quản lý</a>
                        @endif

                    </p>
                @endif

            </div>
        </div>
    </div>
</div>
<div id="menu">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-12">
                <ul>
                    <li><a href="{{route('index')}}"><i class="fa fa-home"></i> Trang chủ</a></li>
                    {{--<li><a href="{{route('gold')}}"><i class="fa fa-dollar"></i> Mua vàng</a></li>--}}
                    {{--<li><a href="{{route('gem')}}"><i class="fa fa-diamond"></i> Mua ngọc</a></li>--}}
                    <li><a href="{{route('shop')}}" class="shop"><i class="fa fa-shopping-basket"></i> Shop acc</a></li>
                    <li><a href="{{route('card')}}"><i class="fa fa-credit-card-alt"></i> Nạp thẻ</a></li>
                    @if(\Illuminate\Support\Facades\Auth::check())
                    <li><a href="{{route('send')}}"><i class="fa fa-money"></i> Chuyển tiền thành viên</a></li>
                        @endif
                </ul>
            </div>
        </div>
    </div>
</div>
@yield('content')
<footer class="text-white">
    <div class="container">
        <div class="row pt-3 pb-1">
            <div class="col-sm-4">
                <img src="images/flogo.png" alt="" class="img-fluid mb-3">
                <p>Bản quyền thuộc về lung2linh shop. Mọi thông tin đăng tải tại đây đều bảo hộ bởi lung2linh shop. Vui
                    lòng liên hệ để admin@lung2linh.vn để cung cấp thông tin. Xin cám ơn</p>
            </div>
            <div class="col-sm-4">
                <h4 class="text-uppercase">Liên hệ</h4>
                <hr>
                <p>Website: www.lung2linh.vn</p>
                <p>Hotline: {{$system->hot_line}}</p>
                <p>Facebook: <a href="{{$system->facebook}}"><i class="fa fa-facebook-official"></i> / lung2linh</p></a>
                <p>Youtube: <a href="{{$system->youtube}}"><i class="fa fa-youtube"></i> / lung2linh</p></a>
            </div>

            <div class="col-12">
                <p class="text-center "><small class="text-white">© Thiết kế bởi : <a href="https://fb.com/nhatquangvu89">nhatquang.vu</a></small></p>
            </div>
        </div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="js/bootstrap.bundle.js"></script>

@yield('script')
</body>
</html>