@extends('template')
@section('content')
    <div id="login">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 offset-sm-3">
                    <h4 class="header"><img src="images/mini-icon.png" alt=""> Đăng ký</h4>
                    <div class="bg-trans p-4">
                        <form action="{{route('register')}}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Tên đăng nhập</label>
                                <input type="text" class="form-control" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Mật khẩu</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group">
                                <label for="" class="text-white text-uppercase">Gõ lại Mật khẩu</label>
                                <input type="password" class="form-control" name="repassword" required>
                            </div>
                            {{csrf_field()}}
                            <button class="btn btn-login btn-block text-uppercase text-white" type="submit"><i class="fa fa-registered"></i> Đăng ký</button>
                        </form>
                        @if(\Illuminate\Support\Facades\Session::has('notice'))
                            <br>
                            <p class="bg-success text-white p-3">
                              <i class="fa fa-info"></i>  {{\Illuminate\Support\Facades\Session::get('notice')}}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection