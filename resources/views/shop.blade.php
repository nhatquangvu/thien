@extends('template')
@section('content')
    <div id="gold">
        <div class="container p-0">
            <div class="row m-0 p-0">
                @if(\Illuminate\Support\Facades\Session::has('notice'))
                    <div class="col-sm-12 m-0 p-0">
                        <p class="bg-success text-white p-3">
                            {{\Illuminate\Support\Facades\Session::get('notice')}}
                        </p>
                    </div>
                @endif
                <form action="">
                    <div class="col-sm-12 m-0 p-0 bg-trans p-3" id="shop-search">
                        <h6 class="text-white"><i class="fa fa-search"></i> Tìm kiếm</h6>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Mã số</div>
                                    </div>
                                    <input type="text" class="form-control" name="numb">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Mức giá</div>
                                    </div>
                                    <select name="price" id="" class="form-control">
                                        <option value="0,1000000000">Không giới hạn</option>
                                        <option value="0,100000">Từ 0 - 100K</option>
                                        <option value="100000,300000">Từ 100k - 300K</option>
                                        <option value="300000,500000">Từ 300k - 500K</option>
                                        <option value="500000,1000000">Từ 500K - 1Tr</option>
                                        <option value="1000000,5000000">Từ 1tr - 5Tr</option>
                                        <option value="5000000,50000000">Trên 5Tr</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Server</div>
                                    </div>
                                    <select name="server" id="" class="form-control">
                                        <option value="0">Tất cả</option>
                                        @foreach($servers as $server)
                                            <option value="{{$server->id}}">{{$server->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Hành tinh</div>
                                    </div>
                                    <select name="planet" id="" class="form-control">
                                        <option value="0">Tất cả</option>
                                        @foreach($planets as $planet)
                                            <option value="{{$planet->id}}">{{$planet->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Đăng ký</div>
                                    </div>
                                    <select name="hasGmail" id="" class="form-control">
                                        <option value="0">Tất cả</option>
                                        <option value="1">Full Gmail</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Sơ sinh có đệ</div>
                                    </div>
                                    <select name="hasPract" id="" class="form-control">
                                        <option value="0">Tất cả</option>
                                        <option value="1">Có</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">Bông tai</div>
                                    </div>
                                    <select name="hasRing" id="" class="form-control">
                                        <option value="0">Tất cả</option>
                                        <option value="1">Có</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <button class="btn btn-block btn-warning"><i class="fa fa-search"></i> LỌC</button>
                            </div>

                        </div>
                    </div>
                </form>

                <div class="col-sm-12 p-3 text-white">
                    <div class="row">
                        @foreach($accs as $acc)
                            <div class="col-sm-4 mb-2">
                                <a href="" data-toggle="modal" data-target="#modal-{{$acc->id}}" class="text-white">
                                    <div class="bg-trans2">
                                        <div class="img-acc" style="height: 220px; overflow: hiden">
                                            <img src="{{asset('images/account')}}/{{$acc->image}}" alt="" class="img-fluid">
                                        </div>
                                        <div class="p-2">
                                            <h6 class="color1 text-center font-weight-bold">{{number_format($acc->price)}}</h6>
                                            <span class="font-weight-bold">{{$acc->name}}</span> <br>
                                            <span class="font-weight-bold">Mã số</span> : <span
                                                    class="badge badge-danger">#{{$acc->id}}</span><br>
                                            <span class="font-weight-bold">Server :</span> {{$acc->server->name}}<br>
                                            <span class="font-weight-bold">Hành tinh :</span> {{$acc->planet->name}}<br>
                                            <span class="font-weight-bold">Có đệ :</span> @php if($acc->hasPract ) echo 'Có'; else echo 'Không' @endphp
                                            <br>
                                            <span class="font-weight-bold"> @php if($acc->hasGmail ) echo 'Có'; else echo 'Tài khoản ảo' @endphp </span>
                                            <br>
                                            <span class="font-weight-bold">Bông tai :</span> @php if($acc->hasRing ) echo 'Có'; else echo 'Không' @endphp
                                            <br>
                                        </div>
                                        @if(\Illuminate\Support\Facades\Auth::check())
                                            {{csrf_field()}}
                                            <a href="{{route('shop',['buy' => $acc->id])}}" class="btn btn-xs btn-buy btn-block text-uppercase text-white"><i
                                                        class="fa fa-shopping-bag"></i> Mua ACC
                                            </a>
                                        @else
                                            <a href="{{route('login')}}"
                                               class="btn btn-xs btn-login btn-block text-uppercase text-white"><i
                                                        class="fa fa-sign-in"></i> Đăng nhập
                                            </a>
                                        @endif
                                    </div>
                                </a>
                            </div>
                            <div class="modal fade" id="modal-{{$acc->id}}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel{{$acc->id}}" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title color1"
                                                id="exampleModalLabel{{$acc->id}}">{{$acc->name}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body img-100">
                                            {!! $acc->content !!}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            {!! $accs->appends(request()->input())->links('pagination::bootstrap-4') !!}

                        </div>
                    </div>
                </div>
                @if(\Illuminate\Support\Facades\Auth::check())
                    <div class="col-sm-12 mt-3 m-0 p-0">
                        <h5 class="text-white"><i class="fa fa-id-card"></i> Lịch sử giao dịch</h5>
                        <table class="table table-striped table-dark">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Server</th>
                                <th scope="col">Tài khoản</th>
                                <th scope="col">Mật khẩu</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($myaccs as $myacc)
                                <tr>
                                    <td><span class="badge badge-danger">#{{$myacc->id}}</span></td>
                                    <td>{{$myacc->server->name}}</td>
                                    <td>{{$myacc->account}}</td>
                                    <td>{{$myacc->password}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br>

                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection