<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('name')->default(0);
            $table->integer('server_id')->default(0);
            $table->integer('planet_id')->default(0);
            $table->string('account');
            $table->string('password');
            $table->string('image')->default('');
            $table->bigInteger('price')->default(0);
            $table->boolean('hasRing')->default(0);
            $table->boolean('hasGmail')->default(0);
            $table->boolean('hasPract')->default(0);
            $table->integer('user_id')->default(0);
            $table->longText('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
