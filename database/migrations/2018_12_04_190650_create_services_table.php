<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('server_id');
            $table->integer('planet_id');
            $table->string('account');
            $table->string('password');
            $table->bigInteger('quality');
            $table->bigInteger('balance');
            $table->integer('user_id');
            $table->boolean('isDone')->default(0);
            $table->boolean('isGold')->default(1);
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
