<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', ['as' => 'index', 'uses' => 'FrontendController@index']);
Route::any('/logout', ['as' => 'logout', 'uses' => 'FrontendController@logout']);
Route::any('/login', ['as' => 'login', 'uses' => 'FrontendController@login']);
Route::any('/register', ['as' => 'register', 'uses' => 'FrontendController@register']);
Route::any('/card', ['as' => 'card', 'uses' => 'FrontendController@card']);
//Route::any('/gold', ['as' => 'gold', 'uses' => 'FrontendController@gold']);
//Route::any('/gem', ['as' => 'gem', 'uses' => 'FrontendController@gem']);
Route::any('/shop', ['as' => 'shop', 'uses' => 'FrontendController@shop']);
Route::any('/send', ['as' => 'send', 'uses' => 'FrontendController@send'])->middleware('auth');;


Route::group(['middleware' => 'checkAdmin','prefix' => 'manager', 'as' => 'manager::'],function (){
    Route::any('/member',['as' => 'member', 'uses' => 'BackendController@member']);
    Route::any('/card',['as' => 'card', 'uses' => 'BackendController@card']);
    Route::any('/services',['as' => 'services', 'uses' => 'BackendController@services']);
    Route::any('/shop',['as' => 'shop', 'uses' => 'BackendController@shop']);
    Route::any('/system',['as' => 'system', 'uses' => 'BackendController@system']);
});
